package com.example;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;

@Order(Ordered.LOWEST_PRECEDENCE) //Integer.MAX_VALUE;
public class MyAEnvironmentPostProcessor implements EnvironmentPostProcessor, ApplicationListener<ApplicationEvent>{

  private static final Logger log = LoggerFactory.getLogger(MyAEnvironmentPostProcessor.class);

  @Override
  public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
    log.info("A->postProcessEnvironment");
  }

  @Override
  public void onApplicationEvent(ApplicationEvent event) {
    log.info("A->onApplicationEvent");
  }

}
