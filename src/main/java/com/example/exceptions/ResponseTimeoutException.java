package com.example.exceptions;


public class ResponseTimeoutException extends TechnicalException {
  public ResponseTimeoutException() {
  }

  public ResponseTimeoutException(String message) {
    super(message);
  }

  public ResponseTimeoutException(Throwable cause) {
    super(cause);
  }

  public ResponseTimeoutException(String message, Throwable cause) {
    super(message, cause);
  }

  public ResponseTimeoutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
