package com.example;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

@Slf4j
public class DebugInterceptor implements MethodInterceptor {

  public Object invoke(MethodInvocation invocation) throws Throwable {
    log.info("Debug Before: invocation=[" + invocation + "]");
    Object rval = invocation.proceed();
    log.info("Debug After: Invocation returned");
    return rval;
  }
}