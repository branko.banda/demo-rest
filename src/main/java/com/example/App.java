package com.example;

import com.example.exceptions.BusinessException;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.config.ConfigFileApplicationListener;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@RefreshScope
public class App {

  public static void main(String[] args) {
    ConfigFileApplicationListener configFileApplicationListener;
    SpringApplication.run(App.class, args);
  }

  @RestController
  //@Scope("prototype")
  static class MyController /*implements ApplicationContextAware*/ {
    AtomicInteger counter = new AtomicInteger(0);

//    private final MessageService messageService;
//    public MyController(@Autowired MessageService messageService) {
//      this.messageService = messageService;
//    }

//    private  MessageService messageService;
//    @Autowired
//    public void setMessageService(MessageService messageService) {
//      this.messageService = messageService;
//    }

    private final ObjectFactory<MessageService> messageServiceBuilder;
    MyController(ObjectFactory<MessageService> messageServiceBuilder) {
      this.messageServiceBuilder = messageServiceBuilder;
    }

    @GetMapping("")
    ResponseEntity<String> getRoot() {
      //MessageService messageService = (MessageService) ctx.getBean("messageService");
      //MessageService messageService = getMessageServiceBean();
      MessageService messageService = messageServiceBuilder.getObject();
      System.out.println(this.toString());
      String msg = messageService.getConfigMessage();
      System.out.println("call " + counter.incrementAndGet());
      return ResponseEntity.ok("App OK " + msg);
    }

//    @Lookup
//    public MessageService getMessageServiceBean() {
//      //Do not provide method implementation, spring will override this method behind the scenes.
//      return null;
//    }

    @GetMapping("/ex")
    void getEx() {
      throw new BusinessException("My BusinessException");
    }

//    private ApplicationContext ctx;
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//      this.ctx = applicationContext;
//    }

  }

  @Bean
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public MessageService messageService() {
    return new MessageServiceImpl();
//    return createProxyMessageService(new MessageServiceImpl());
//    return createDebugProxyMessageService(
//      createTracingProxyMessageService(
//        new MessageServiceImpl()));
  }

  private MessageService createDebugProxyMessageService(MessageService originalSvc) {
    ProxyFactory proxyFactory = new ProxyFactory();
    proxyFactory.setTarget(originalSvc);
    proxyFactory.addAdvice(new DebugInterceptor());
    proxyFactory.setInterfaces(MessageService.class); //new Class[]{MessageService.class}
    return (MessageService) proxyFactory.getProxy();
  }

  private MessageService createTracingProxyMessageService(MessageService originalSvc) {
    ProxyFactory proxyFactory = new ProxyFactory();
    proxyFactory.setTarget(originalSvc);
    proxyFactory.addAdvice(new TracingInterceptor());
    proxyFactory.setInterfaces(MessageService.class);
    return (MessageService) proxyFactory.getProxy();
  }

  // both
  private MessageService createProxyMessageService(MessageService originalSvc) {
    ProxyFactory proxyFactory = new ProxyFactory();
    proxyFactory.setTarget(originalSvc);
    proxyFactory.addAdvice(0, new DebugInterceptor());
    proxyFactory.addAdvice(1, new TracingInterceptor());
    proxyFactory.setInterfaces(MessageService.class);
    //proxyFactory.setOpaque(true);
    return (MessageService) proxyFactory.getProxy();
  }

  interface MessageService {
    String getConfigMessage();
  }

  //@Service
  static class MessageServiceImpl implements MessageService {
    @Value("${message:default message}")
    private String msg;

    @GetMapping("")
    public String getConfigMessage() {
      System.out.println(this.toString());
      return msg;
    }
  }

}
