package com.example.aspects;

import org.aspectj.lang.annotation.Pointcut;

public class Pointcuts {

  // Pointcut definition to display all the available methods i.e. the advice will be called for all the methods.
  // Pointcut expressions can be combined using '&&', '||' and '!'.
  // proxy object itself (bound to 'this') and the target object behind the proxy (bound to 'target').
/*
execution - for matching method execution join points, this is the primary pointcut designator you will usewhen working with Spring AOP
 @Pointcut("execution(public * *(..))")

within - limits matching to join points within certain types (simply the execution of a method declared within a matching type when using Spring AOP)
 @Pointcut("within(com.xyz.someapp.trading..*)")

this - limits matching to join points (the execution of methods when using Spring AOP) where the bean reference (Spring AOP proxy) is an instance of the given type
 @Pointcut("this(com.xyz.service.AccountService)")

target - limits matching to join points (the execution of methods when using Spring AOP) where the target object (application object being proxied) is an instance of the given type
 @Pointcut("target(com.xyz.service.AccountService)")

args - limits matching to join points (the execution of methods when using Spring AOP) where the arguments are instances of the given types
 @Pointcut("args(java.io.Serializable)")

@target - limits matching to join points (the execution of methods when using Spring AOP) where the class of the executing object has an annotation of the given type
 @target(org.springframework.transaction.annotation.Transactional)
 - any join point where the target object has an @Transactional annotation:

@args - limits matching to join points (the execution of methods when using Spring AOP) where the runtime type of the actual arguments passed have annotations of the given type(s)
 @args(com.xyz.security.Classified)
 - any join point which takes a single parameter, and where the runtime type of the argument passed has the @Classified annotation:

@within - limits matching to join points within types that have the given annotation (the execution of methods declared in types with the given annotation when using Spring AOP)
  @within(org.springframework.transaction.annotation.Transactional)
  - any join point where the declared type of the target object has an @Transactional annotation:

@annotation - limits matching to join points where the subject of the join point (method being executed in Spring AOP) has the given annotation
 @annotation(org.springframework.transaction.annotation.Transactional)
 - any join point where the executing method has an @Transactional annotation

*/
  @Pointcut(value = "execution(* com.example.App.*.* (..))")
  protected void exceptionAfterExternalServiceCall() {
  }

}
