package com.example.aspects;

import com.example.exceptions.ApiException;
import com.example.exceptions.AuthenticationException;
import com.example.exceptions.BusinessException;
import com.example.exceptions.ConnectionTimeoutException;
import com.example.exceptions.EntityNotFoundException;
import com.example.exceptions.ResponseTimeoutException;
import com.example.exceptions.TechnicalException;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.net.ConnectException;

@Aspect
@Component
@Slf4j
@Order(Ordered.LOWEST_PRECEDENCE - 1)
public class ExceptionHandling extends Pointcuts {

    // Method is executed after the method matching with a pointcut expression.
    @AfterThrowing(value = "exceptionAfterExternalServiceCall()", throwing = "exception")
    public void afterThrowingAdvice(JoinPoint jp, Throwable exception) throws ApiException {
        log.error("Method {} raised an exception",jp.getSignature().getName()); //, exception
        handleExceptions((Exception) exception);
    }

    private void handleExceptions(Exception e) throws ApiException {
        if (e instanceof java.net.SocketTimeoutException) {
            throw new ResponseTimeoutException(e);
        }
        Throwable t = NestedExceptionUtils.getMostSpecificCause(e);
        if (t instanceof ConnectException) {
            throw new ConnectionTimeoutException(e);
        }
        if (t instanceof EntityNotFoundException) {
            throw new EntityNotFoundException(t.getMessage());
        }
        if (t instanceof AuthenticationException) {
            throw new AuthenticationException(t.getMessage());
        }

        if (t instanceof BusinessException) {
                throw new BusinessException(t.getMessage());

        }
        if (t instanceof TechnicalException) {
                throw new TechnicalException(t.getMessage());
        }

        throw new ApiException("Unhandled Exception", e);
    }
}
