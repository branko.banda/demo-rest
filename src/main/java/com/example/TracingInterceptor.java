package com.example;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

@Slf4j
public class TracingInterceptor implements MethodInterceptor {
  @Override
  public Object invoke(MethodInvocation i) throws Throwable {
    log.info("Tracing before: method " + i.getMethod() + " is called on " + i.getThis() + " with args " + i.getArguments());
    Object ret = i.proceed();
    log.info("Tracing after: method " + i.getMethod() + " returns " + ret);
    return ret;
  }
}