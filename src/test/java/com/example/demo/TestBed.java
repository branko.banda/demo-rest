package com.example.demo;

public class TestBed {
  public void f() {
    System.out.println("f()");
  }

  public static class Tester {
    public static void main(String[] args) {
      TestBed t = new TestBed();
      t.f();
    }
  }
}
/*
 from dir src/test/java: java com.example.demo.TestBed$Tester
Output: f()
*/

