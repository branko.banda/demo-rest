package com.example.demo.closure;

import java.util.function.IntSupplier;
import java.util.function.Supplier;

public class Closure3 {
  int i;

  IntSupplier makeFun(int x) {
    int i = 0;
    i++;
    x++;
    final int iFinal = i;
    final int xFinal = x;
    return () -> xFinal + (iFinal);
  }

  Supplier<Integer> makeFunSupplier(int x) {
    int i = 0;
    i++;
    x++;
    final int iFinal = i;
    final int xFinal = x;
    return () -> xFinal + (iFinal);
  }

  public static void main(String[] args) {
    Closure3 c = new Closure3();
    IntSupplier i1 = c.makeFun(1);
    IntSupplier i2 = c.makeFun(2);
    IntSupplier i3 = c.makeFun(3);
    Supplier<Integer> i4 = c.makeFunSupplier(4);

    System.out.println(i1.getAsInt());
    System.out.println(i2.getAsInt());
    System.out.println(i3.getAsInt());
    System.out.println(i4.get());

  }

}
