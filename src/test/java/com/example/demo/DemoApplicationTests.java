package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertEquals;

//https://stackify.com/streams-guide-java-8/


@SpringBootTest
class DemoApplicationTests {

  @Test
  void contextLoads() {
  }

  @Test
  void faceBeanLoads() {
    PropertyChangeListener listener = new MyPropertyChangeListener();

    FaceBean fb = new FaceBean(); //def mountWidth 90
    fb.addPropertyChangeListener(listener);
    fb.setMouthWidth(100);
    fb.removePropertyChangeListener(listener);
    fb.setMouthWidth(80);
    System.out.println("****");
  }

  @Test
  public void whenFlatMapOneLevel_thenGetNameStream() {
    List<String> namesOneLevel = Arrays.asList(
      "Jeff",
      "Bill",
      "Mark"
    );

    List<Character> flattened =
      namesOneLevel.stream()
        .flatMap(s -> s.chars().mapToObj(c -> (char) c)).collect(Collectors.toList());

    flattened.forEach(System.out::println);

  }

  @Test
  public void whenFlatMapTwoLevel_thenGetNameStream() {
    List<List<String>> namesOneLevel = Arrays.asList(
      Arrays.asList("Jeff"),
      Arrays.asList("Bill"),
      Arrays.asList("Mark")
    );

    List<String> flattened = namesOneLevel.stream()
      .flatMap(Collection::stream)
      .collect(Collectors.toList());

    flattened.forEach(System.out::println);
  }

  @Test
  public void whenFlatMapThreeLevel_thenGetNameStream() {
    List<List<List<String>>> namesNested = Arrays.asList(
      Arrays.asList(Arrays.asList("Jeff", "Bezos"), Arrays.asList("Amazon", "AZ", "1")),
      Arrays.asList(Arrays.asList("Bill", "Gates"), Arrays.asList("Microsoft", "MS", "3")),
      Arrays.asList(Arrays.asList("Mark", "Zuckerberg"), Arrays.asList("Facebook", "FB", "2"))
    );

    List<List<String>> namesNestedFlatMap = Arrays.asList(
      Arrays.asList("Jeff", "Bezos"), Arrays.asList("Amazon", "AZ", "1"),
      Arrays.asList("Bill", "Gates"), Arrays.asList("Microsoft", "MS", "3"),
      Arrays.asList("Mark", "Zuckerberg"), Arrays.asList("Facebook", "FB", "2")
    );

    List<List<String>> list = namesNested.stream()
      .flatMap(Collection::stream)
      .collect(Collectors.toList());

    System.out.println(list);
    for (List<String> sublist : list) {
      sublist.forEach(System.out::println);
      System.out.println("-----------");
    }
    //assertEquals(namesFlatStream.size(), namesNested.size() * 2);
  }

  @Test
  public void whenFlatMapEmployeeNames_thenGetNameStream() {
    List<List<String>> namesNested = Arrays.asList(
      Arrays.asList("Jeff", "Bezos"),
      Arrays.asList("Bill", "Gates"),
      Arrays.asList("Mark", "Zuckerberg"));

    List<String> namesFlatStream = namesNested.stream()
      .flatMap(Collection::stream)
      .collect(Collectors.toList());

    assertEquals(namesFlatStream.size(), namesNested.size() * 2);
  }

  @Test
  public void whenIncrementSalaryUsingPeek_thenApplyNewSalary() {
    Employee[] arrayOfEmps = {
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    };

    List<Employee> empList = Arrays.asList(arrayOfEmps);

    empList.stream()
      .peek(e -> e.salaryIncrement(10.0))
      .peek(System.out::println)
      .collect(Collectors.toList());

    assertThat(empList, contains(
      hasProperty("salary", equalTo(110_000.0)),
      hasProperty("salary", equalTo(220_000.0)),
      hasProperty("salary", equalTo(330_000.0))
    ));
  }

  @Test
  public void whenLimitInfiniteStream_thenGetFiniteElements() {
    Stream<Integer> infiniteStream = Stream.iterate(2, i -> i * 2);

    List<Integer> collect = infiniteStream
      .skip(3)
      .limit(5)
      .collect(Collectors.toList());

    assertEquals(collect, Arrays.asList(16, 32, 64, 128, 256));
  }

  @Test
  public void whenFindFirst_thenGetFirstEmployeeInStream() {
    EmployeeRepository employeeRepository = new EmployeeRepository();

    Integer[] empIds = {1, 2, 3, 4};

    Employee employee = Stream.of(empIds)
      .map(employeeRepository::findById)
      .filter(e -> e != null)
      .filter(e -> e.getSalary() > 100000)
      .findFirst()
      .orElse(null);

    assertEquals(employee.getSalary(), new Double(200000));
  }

  @Test
  public void whenSortStream_thenGetSortedStream() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    List<Employee> employees = empList.stream()
      .sorted((e1, e2) -> e1.getName().compareTo(e2.getName()))
      .collect(Collectors.toList());

    assertEquals(employees.get(0).getName(), "Bill Gates");
    assertEquals(employees.get(1).getName(), "Jeff Bezos");
    assertEquals(employees.get(2).getName(), "Mark Zuckerberg");
  }

  @Test
  public void whenFindMin_thenGetMinElementFromStream() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(2, "Jeff Bezos", 100000.0),
      new Employee(3, "Bill Gates", 200000.0),
      new Employee(4, "Mark Zuckerberg", 300000.0)
    ));

    Employee firstEmp = empList.stream()
      .min((e1, e2) -> e1.getId() - e2.getId())
      .orElseThrow(NoSuchElementException::new);

    assertEquals(firstEmp.getId(), new Integer(2));

    Employee maxSalEmp = empList.stream()
      .max(Comparator.comparing(Employee::getSalary))
      .orElseThrow(NoSuchElementException::new);

    assertEquals(maxSalEmp.getSalary(), new Double(300000.0));
  }


  @Test
  public void whenApplyDistinct_thenRemoveDuplicatesFromStream() {
    List<Integer> intList = Arrays.asList(2, 5, 3, 2, 4, 3);
    List<Integer> distinctIntList = intList.stream().distinct().collect(Collectors.toList());

    assertEquals(distinctIntList, Arrays.asList(2, 5, 3, 4));
  }

  @Test
  public void whenApplyMatch_thenReturnBoolean() {
    List<Integer> intList = Arrays.asList(2, 4, 5, 6, 8);

    boolean allEven = intList.stream().allMatch(i -> i % 2 == 0);
    boolean atLeastOneEven = intList.stream().anyMatch(i -> i % 2 == 0);
    long numEven = intList.stream().filter(i -> i % 2 == 0).count();
    boolean noneMultipleOfThree = intList.stream().noneMatch(i -> i % 3 == 0);

    assertEquals(allEven, false);
    assertEquals(atLeastOneEven, true);
    assertEquals(numEven, 4);
    assertEquals(noneMultipleOfThree, false);
  }

  @Test
  public void whenFindMaxOnIntStream_thenGetMaxInteger() {
    // Stream<Integer> isn't IntStream.
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Integer latestEmpId = empList.stream()
      .mapToInt(Employee::getId)
      .max()
      .orElseThrow(NoSuchElementException::new);

    assertEquals(latestEmpId, new Integer(3));
  }

  @Test
  public void whenApplySumOnIntStream_thenGetSum() {
    // Specialized Operations:  sum(), average(), range()
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Double avgSal = empList.stream()
      .mapToDouble(Employee::getSalary)
      .average()
      .orElseThrow(NoSuchElementException::new);

    assertEquals(avgSal, new Double(200000));
  }


  @Test
  public void whenApplyReduceOnStream_thenGetValue() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Double sumSal = empList.stream()
      .map(Employee::getSalary)
      .reduce(0.0, Double::sum);

    assertEquals(sumSal, new Double(600000));
  }

  @Test
  public void whenCollectByJoining_thenGetJoinedString() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    String empNames = empList.stream()
      .map(Employee::getName)
      .collect(Collectors.joining(", ")) //internally uses a java.util.StringJoiner to perform the joining operation
      .toString();

    assertEquals(empNames, "Jeff Bezos, Bill Gates, Mark Zuckerberg");
  }

  @Test
  public void whenCollectBySet_thenGetSet() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));
    Set<String> empNames = empList.stream()
      .map(Employee::getName)
      .collect(Collectors.toSet());

    assertEquals(empNames.size(), 3);
  }

  @Test
  public void whenToVectorCollection_thenGetVector() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));
    Vector<String> empNames = empList.stream()
      .map(Employee::getName)
      .collect(Collectors.toCollection(Vector::new));

    assertEquals(empNames.size(), 3);
  }

  @Test
  public void whenApplySummarizing_thenGetBasicStats() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    DoubleSummaryStatistics stats = empList.stream()
      .collect(Collectors.summarizingDouble(Employee::getSalary));

    assertEquals(stats.getCount(), 3);
    assertEquals(stats.getSum(), 600000.0, 0);
    assertEquals(stats.getMin(), 100000.0, 0);
    assertEquals(stats.getMax(), 300000.0, 0);
    assertEquals(stats.getAverage(), 200000.0, 0);
  }

  @Test
  public void whenApplySummaryStatistics_thenGetBasicStats() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    DoubleSummaryStatistics stats = empList.stream()
      .mapToDouble(Employee::getSalary)
      .summaryStatistics();

    assertEquals(stats.getCount(), 3);
    assertEquals(stats.getSum(), 600000.0, 0);
    assertEquals(stats.getMin(), 100000.0, 0);
    assertEquals(stats.getMax(), 300000.0, 0);
    assertEquals(stats.getAverage(), 200000.0, 0);
  }

  @Test
  public void whenStreamPartition_thenGetMap() {
    List<Integer> intList = Arrays.asList(2, 4, 5, 6, 8);
    Map<Boolean, List<Integer>> isEven = intList.stream().collect(
      Collectors.partitioningBy(i -> i % 2 == 0));

    assertEquals(isEven.get(true).size(), 4);
    assertEquals(isEven.get(false).size(), 1);
    assertEquals(isEven.get(false), new ArrayList<Integer>(Collections.singletonList(5)));
  }

  @Test
  public void whenStreamGroupingBy_thenGetMap() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Map<Character, List<Employee>> groupByAlphabet = empList.stream().collect(
      Collectors.groupingBy(e -> new Character(e.getName().charAt(0))));

    assertEquals(groupByAlphabet.get('B').get(0).getName(), "Bill Gates");
    assertEquals(groupByAlphabet.get('J').get(0).getName(), "Jeff Bezos");
    assertEquals(groupByAlphabet.get('M').get(0).getName(), "Mark Zuckerberg");
  }

  @Test
  public void whenStreamMapping_thenGetMap() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Map<Character, List<Integer>> idGroupedByAlphabet = empList.stream().collect(
      Collectors.groupingBy(e -> new Character(e.getName().charAt(0)),
        Collectors.mapping(Employee::getId, Collectors.toList())));

    assertEquals(idGroupedByAlphabet.get('B').get(0), new Integer(2));
    assertEquals(idGroupedByAlphabet.get('J').get(0), new Integer(1));
    assertEquals(idGroupedByAlphabet.get('M').get(0), new Integer(3));
  }

  @Test
  public void whenStreamReducing_thenGetValue() {
    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Double percentage = 10.0;
    Double salIncrOverhead = empList.stream()
      .collect(Collectors.reducing(
        0.0, e -> e.getSalary() * percentage / 100, (s1, s2) -> s1 + s2));

    assertEquals(salIncrOverhead, 60000.0, 0);
  }

  @Test
  public void whenStreamGroupingAndReducing_thenGetMap() {

    List<Employee> empList = new ArrayList<Employee>(Arrays.asList(
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    ));

    Comparator<Employee> byNameLength = Comparator.comparing(Employee::getName);

    Map<Character, Optional<Employee>> longestNameByAlphabet = empList.stream().collect(
      Collectors.groupingBy(e -> new Character(e.getName().charAt(0)),
        Collectors.reducing(BinaryOperator.maxBy(byNameLength))));

    assertEquals(longestNameByAlphabet.get('B').get().getName(), "Bill Gates");
    assertEquals(longestNameByAlphabet.get('J').get().getName(), "Jeff Bezos");
    assertEquals(longestNameByAlphabet.get('M').get().getName(), "Mark Zuckerberg");
  }

  @Test
  public void whenParallelStream_thenPerformOperationsInParallel() {
    Employee[] arrayOfEmps = {
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    };

    List<Employee> empList = Arrays.asList(arrayOfEmps);

    empList.stream().parallel().forEach(e -> e.salaryIncrement(10.0));

    assertThat(empList, contains(
      hasProperty("salary", equalTo(110000.0)),
      hasProperty("salary", equalTo(220000.0)),
      hasProperty("salary", equalTo(330000.0))
    ));
  }

  @Test
  public void whenGenerateStream_thenGetInfiniteStream() {
    Stream.generate(Math::random)
      .limit(5)
      .forEach(System.out::println);
  }

  @Test
  public void whenIterateStream_thenGetInfiniteStream() {
    Stream<Integer> evenNumStream = Stream.iterate(2, i -> i * 2);

    List<Integer> collect = evenNumStream
      .limit(5)
      .collect(Collectors.toList());

    assertEquals(collect, Arrays.asList(2, 4, 8, 16, 32));
  }

  @Test
  public void whenStreamToFile_thenGetFile() throws IOException {
    String fileName = "myfile.txt";
    String[] words = {
      "hello",
      "refer",
      "world",
      "level"
    };

    try (PrintWriter pw = new PrintWriter(
      Files.newBufferedWriter(Paths.get(fileName)))) {
      Stream.of(words).forEach(pw::println);
    }
  }

  private List<String> getPalindrome(Stream<String> stream, int length) {
    return stream.filter(s -> s.length() == length)
      .filter(s -> s.compareToIgnoreCase(
        new StringBuilder(s).reverse().toString()) == 0)
      .collect(Collectors.toList());
  }

  @Test
  public void whenFileToStream_thenGetStream() throws IOException {
    String fileName = "myfile.txt";
    List<String> str = getPalindrome(Files.lines(Paths.get(fileName)), 5);
    assertThat(str, contains("refer", "level"));
  }

  //
  public static class Employee {
    private int id;
    private String name;
    private double salary;

    public Employee(int id, String name, double salary) {
      this.id = id;
      this.name = name;
      this.salary = salary;
    }

    public void salaryIncrement(double percentIncrement) {
      int a = (int) (this.salary * (1 + percentIncrement / 100)
        * 100);
      this.salary = (double) a / 100;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public double getSalary() {
      return salary;
    }
  }


  private class MyPropertyChangeListener implements PropertyChangeListener {
    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
      System.out.println("** propertyChangeEvent:" +
        propertyChangeEvent.getPropagationId() + "|" +
        propertyChangeEvent.getPropertyName() + "|" +
        propertyChangeEvent.getOldValue() + "|" +
        propertyChangeEvent.getNewValue()
      );
    }
  }

  private class EmployeeRepository {
    Employee[] arrayOfEmps = {
      new Employee(1, "Jeff Bezos", 100000.0),
      new Employee(2, "Bill Gates", 200000.0),
      new Employee(3, "Mark Zuckerberg", 300000.0)
    };

    public Employee findById(int index) {
      return arrayOfEmps[index];
    }
  }
}
