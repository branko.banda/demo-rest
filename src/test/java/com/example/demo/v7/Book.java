package com.example.demo.v7;

class Book
{
   private String title;
   private int pubYear; // publication year
   private String[] authors;

   static int count;

   Book(String title)
   {
      this(title, -1);

      // Do not include ++count; here because it already
      // executes in the second constructor and would 
      // execute here after this() returns. You would end
      // up with one extra book in the count.
   }

   Book(String title, int pubYear)
   {
      this(title, pubYear, "");
   }

   Book(String title, int pubYear, String author)
   {
      this(title, pubYear, new String[] { author });
   }

   Book(String title, int pubYear, String[] authors)
   {
      setTitle(title);
      setPubYear(pubYear);
      setAuthors(authors);
      ++count;
   }

   String getTitle()
   {
      return title;
   }

   int getPubYear()
   {
      return pubYear;
   }

   String getAuthor()
   {
      return authors[0];
   }

   String[] getAuthors()
   {
      return authors;
   }

   void setTitle(String title)
   {
      this.title = title;
   }

   void setPubYear(int pubYear)
   {
      this.pubYear = pubYear;
   }

   void setAuthor(String author)
   {
      setAuthors(new String[] { author });
   }

   void setAuthors(String[] authors)
   {
      this.authors = authors;
   }

   static void showCount()
   {
      System.out.println("count = " + count);
   }

   public void changeBookEd(Book book){
      book.title = book.title + " .";
   }
}