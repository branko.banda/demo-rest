package com.example.demo.v7;

class Books
{
   public static void main(String[] args)
   {
      Book book = new Book("Grimms' Fairy Tales", 1812, 
                           new String[] { "Jacob Grimm", "Wilhelm Grimm" });
      System.out.println(book.getTitle()); // Output: Grimms' Fairy Tales

      System.out.println(book.getPubYear()); // Output: 1812
      System.out.println(book.getAuthor()); // Output: Jacob Grimm
      String[] authors = book.getAuthors();
      for (int i = 0; i < authors.length; i++)
         System.out.println(authors[i]);

      book.changeBookEd(book);
      book.changeBookEd(book);
      book.changeBookEd(book);
      System.out.println(book.getTitle());

   }
}