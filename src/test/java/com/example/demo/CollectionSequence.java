package com.example.demo;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

public class CollectionSequence extends AbstractCollection<Pet> {
  private Pet[] pets = new Pet[8];

  public CollectionSequence() {
    for (int i = 0; i < pets.length; i++) {
      pets[i] = new Pet();
    }
  }

  @Override
  public int size() {
    return pets.length;
  }

  @Override
  public Iterator<Pet> iterator() {
    return new Iterator<Pet>() {
      private int index = 0;

      @Override
      public boolean hasNext() {
        return index < pets.length;
      }

      @Override
      public Pet next() {
        return pets[index++];
      }

      @Override
      public void remove() { // Not implemented
        throw new UnsupportedOperationException();
      }
    };
  }

  public static void main(String[] args) {
    CollectionSequence c = new CollectionSequence();
    InterfaceVsIterator.display(c);
    InterfaceVsIterator.display(c.iterator());
  }
}

class Pet {
  private static int counter;

  public Pet() {
    this.id = String.format("pet-%d", counter++);
  }

  private String id;

  public String id() {
    return id;
  }

  @Override
  public String toString() {
    return "Pet{" + "id='" + id + '\'' + '}';
  }
}

class InterfaceVsIterator {
  public static void display(Iterator<Pet> it) {
    while (it.hasNext()) {
      Pet p = it.next();
      System.out.print(p.id() + ":" + p + " ");
    }
    System.out.println();
  }

  public static void display(Collection<Pet> pets) {
    for (Pet p : pets)
      System.out.print(p.id() + ":" + p + " ");
    System.out.println();
  }
}