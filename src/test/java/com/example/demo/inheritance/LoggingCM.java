package com.example.demo.inheritance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingCM extends CM {

  private static final Logger log = LoggerFactory.getLogger(LoggingCM.class);

  @Override
  public void addContact(Contact contact)
  {
    log.info(contact.toString());
    super.addContact(contact);
  }

  @Override
  public void addContacts(Contact[] contacts)
  {
    for (int i = 0; i < contacts.length; i++)
      log.info(contacts[i].toString());
    super.addContacts(contacts);
  }
}