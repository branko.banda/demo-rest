package com.example.demo.inheritance;

public class CMDemo {
  public static void main(String[] args) {
    Contact[] contacts = {new Contact(), new Contact(), new Contact()};
    LoggingCM lcm = new LoggingCM();
    lcm.addContacts(contacts);

    Contact[] backArr = lcm.getContacts();
    System.out.println("backArr: " + backArr.length);
  }
}
