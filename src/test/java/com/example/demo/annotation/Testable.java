package com.example.demo.annotation;


public class Testable {
  public void execute() {
    System.out.println("Executing..");
  }
  @TestMe
  void testExecute() { execute(); }
}