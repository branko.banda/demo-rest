package com.example.demo.patterns;

import java.util.stream.IntStream;

public abstract class ApplicationFramework {

  public ApplicationFramework() {
    templateMethod();
  }

  abstract void customize1();

  abstract void customize2();

  // "private" means automatically "final":
  private void templateMethod() {
    IntStream.range(0, 5).forEach(
      n -> {
        customize1();
        customize2();
      });
  }
}

class MyApp1 extends ApplicationFramework {
  @Override
  void customize1() {
    System.out.print("Hello ");
  }

  @Override
  void customize2() {
    System.out.println("World!");
  }
}
class MyApp2 extends ApplicationFramework {
  @Override
  void customize1() {
    System.out.print("Bok ");
  }

  @Override
  void customize2() {
    System.out.println("svijete!");
  }
}

class TemplateMethod {
  public static void main(String[] args) {
    new MyApp1();
    new MyApp2();
  }
}