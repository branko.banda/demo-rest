package com.example.demo.patterns;

import java.util.Arrays;
import java.util.List;

/*
Function Object in its purest sense: a method that’s an object.
 You pass a Function Object to a method or an object as a parameter, to vary the operation.
 Before Java 8, to produce the effect of a standalone functions you had
to explicitly wrap a method into an object
 */
public class CommandPattern {

  public static void main(String[] args) {
    List<Runnable> macro = Arrays.asList(
      () -> System.out.print("Hello "),
      () -> System.out.print("World! "),
      () -> System.out.print("I'm the command pattern!")
    );
    macro.forEach(Runnable::run);
  }
}