package com.example.demo;

// Widening(upcasting) and narrowing(downcasting) conversions
//
// covariant : return type
// Contravariant parameter type.
//
// if Cat" is a subtype of "Animal"
// and "list of Cat" is a subtype of "list of Animal" because the list constructor is covariant.
// This means that the subtyping relation of the simple types are preserved for the complex types.
//
//  "function from Animal to String" is a subtype of "function from Cat to String" because the function type constructor is contravariant in the parameter type.

/*
Within the type system of a programming language, a typing rule or a type constructor is:
- covariant if it preserves the ordering of types (≤), which orders types from more specific to more generic;
- contravariant if it reverses this ordering;
- bivariant if both of these apply (i.e., both I<A> ≤ I<B> and I<B> ≤ I<A> at the same time)[1];
- variant if covariant, contravariant or bivariant.
- invariant or nonvariant if not variant.
*/

public class CovariantDemo {
  public static void main(String[] args) {

    Event3D e3d = new Event3D();
    //Point2D p3d = e3d.getLocation();
    Point3D p3d = e3d.getLocation();
    System.out.println(p3d);

  }
}


class Point2D {
  int x, y;

  @Override
  public String toString() {
    return "Point2D{" + "x=" + x + ", y=" + y + '}';
  }
}

class Point3D extends Point2D {
  int z;

  @Override
  public String toString() {
    return "Point3D{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
  }
}

class Event2D {
  public Point2D getLocation() {
    return new Point2D();
  }
}

class Event3D extends Event2D {
  //overriding method may narrow the return type of the method it overrides
  @Override
  public Point3D getLocation() {
    return new Point3D();
  }
}