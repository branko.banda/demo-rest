package com.example.demo;

public class LambdaDemo
{
  public static void main(String[] args)
  {
    new LambdaDemo().doWork();
  }
  public void doWork()
  {
    System.out.printf("this M = %s%n", this);
    Runnable r = new Runnable()
    {
      @Override
      public void run()
      {
        System.out.printf("this R = %s%n", this);
      }
    };
    new Thread(r).start();
    new Thread(() -> System.out.printf("this T = %s%n", this)).start();
  }
}