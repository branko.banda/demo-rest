package com.example.demo;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadIdDemo {

  public static void main(String[] args) {
    System.out.println("main started running.");

    for (int i = 0; i < 3; i++) {
      int tid = ThreadId.get();
      MyThread ct = new MyThread(tid);
      ct.start();
    }
    try {
      Thread.sleep(10_000);
    } catch (InterruptedException e) {
    }
    System.out.println("main started stopping.");
  }
}

class MyThread extends Thread {

  int tid;

  public MyThread(int tid) {
    this.tid = tid;
  }

  public void run() {
    System.out.println(currentThread().getName() + " " + tid + " CalculatingThread running.");
    try {
      Thread.sleep(1_000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(currentThread().getName() + " " + tid + " CalculatingThread running..");
    try {
      Thread.sleep(1_000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(currentThread().getName() + " " + tid + " CalculatingThread running...");
  }
}


class ThreadId {
  // Atomic integer containing the next thread ID to be assigned
  private static final AtomicInteger nextId = new AtomicInteger(0);

  // Thread local variable containing each thread's ID
  private static final ThreadLocal<Integer> threadId = new ThreadLocal<Integer>() {
    @Override
    protected Integer initialValue() {
      return nextId.getAndIncrement();
    }
  };

  // Returns the current thread's unique ID, assigning it if necessary
  public static int get() {
    return threadId.get();
  }
}
/*
 * Do Not Use ThreadLocal With ExecutorService!
 * to specify an initial value for ThreadLocal variable:  create a subclass of ThreadLocal which overrides its initialValue()
    or use its static factory method ThreadLocal.withInitial(Supplier)

 class ThreadLocal ...
 public T get() {
    Thread t = Thread.currentThread();
    ThreadLocal.ThreadLocalMap tmap = this.getMap(t);
    if (tmap != null) {
      ThreadLocal.ThreadLocalMap.Entry tmap_entry = tmap.getEntry(this);
      if (tmap_entry != null) {
        Object value = tmap_entry.value;
        return value;
      }
    }

    return this.setInitialValue();
  }
 */