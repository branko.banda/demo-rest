package com.example.demo.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NapTask implements Runnable {
  final int id;

  public NapTask(int id) {
    this.id = id;
  }

  @Override
  public void run() {
    new Nap(0.1); // Seconds
    log.info(this + " " + Thread.currentThread().getName());
  }

  @Override
  public String toString() {
    return "NapTask[" + id + "]";
  }
}
