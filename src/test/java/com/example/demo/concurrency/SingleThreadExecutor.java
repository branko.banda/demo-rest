package com.example.demo.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@Slf4j
public class SingleThreadExecutor {

  public static void main(String[] args) {

    ExecutorService exec = Executors.newSingleThreadExecutor();

    IntStream.range(0, 10).mapToObj(NapTask::new).forEach(exec::execute);

    log.info("All tasks submitted");
    exec.shutdown();

    while (!exec.isTerminated()) {
      log.info(Thread.currentThread().getName() + " awaiting termination");
      new Nap(0.1);
    }
  }
}
