package com.example.demo;

public class BreakDemo {

  private static final int NUMROWS = 8;
  private static final int NUMCOLS = 8;

  public static void main(String[] args) {
    int[][] data = new int[NUMROWS][NUMCOLS];

    fillArray(data);
//    printArray(data);
    System.out.println("--------------");

    String breakingPoint = "0:0";
    outer:
    for (int row = 0; row < NUMROWS; row++) {
      for (int col = 0; col < NUMCOLS; col++) {
        breakingPoint = row + ":" + col;
        System.out.println(breakingPoint);
        if (data[row][col] == 63) {
          System.out.println("breakingPoint");
          break outer;
        }
      }
    }
    System.out.println("--------------");
    System.out.println(breakingPoint);
  }


  private static void fillArray(int data[][]) {
    for (int row = 0; row < NUMROWS; row++) {
      for (int col = 0; col < NUMCOLS; col++) {
        data[row][col] = (row + 1) * 10 + (col + 1);
      }
    }
  }

  private static void printArray(int data[][]) {
    for (int row = 0; row < NUMROWS; row++) {
      for (int col = 0; col < NUMCOLS; col++) {
        System.out.println(row + "," + col + ":" + data[row][col]);
      }
    }
  }

}
