package com.example.demo;

import lombok.SneakyThrows;

public class ThreadLocalExample {
  @SneakyThrows
  public static void main(String[] args) {
    MyRunnable sharedRunnableInstance = new MyRunnable();
    sharedRunnableInstance.run();

    Thread thread1 = new Thread(sharedRunnableInstance);
    Thread thread2 = new Thread(sharedRunnableInstance);

    thread1.start(); thread2.start();

    thread1.join(); //wait for thread 1 to terminate
    thread2.join(); //wait for thread 2 to terminate
  }
}

class MyRunnable implements Runnable {

  private ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>(); //diff num
  //private Integer threadLocal = 0; // same num
  //private InheritableThreadLocal<Integer> threadLocal = new InheritableThreadLocal<Integer>(); //diff num


  @Override
  public void run() {
    String tn =  Thread.currentThread().getName() + ": ";

    System.out.println(tn + threadLocal.get());

    threadLocal.set( (int) (Math.random() * 100D) );
    //threadLocal = (int) (Math.random() * 100D) ;

    try {Thread.sleep(2000);} catch (InterruptedException e) {}


    System.out.println(tn + threadLocal.get());
    //System.out.println(threadLocal);
  }

}